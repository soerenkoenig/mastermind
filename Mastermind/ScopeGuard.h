﻿#pragma once

#include <functional>

namespace mastermind
{
	/// <summary>
	/// A RAII scope guard to execute a task on destruction. A Task is a functor with signature void func(void).
	/// A scope guard is non-copyable but moveable.
	/// </summary>
	class ScopeGuard final
	{
	public:
		/// <summary>
		/// Creates  empty scope guard (with no task set)
		/// </summary>
		ScopeGuard() = default;

		ScopeGuard(const ScopeGuard&) = delete;

		ScopeGuard(ScopeGuard&& other) noexcept;


		~ScopeGuard();

		ScopeGuard& operator=(const ScopeGuard&) = delete;
		ScopeGuard& operator=(ScopeGuard&& other) noexcept;

		/// <summary>
		/// creates a scope guard with given task to be called on destruction.
		/// </summary>
		/// <param name="onDestroyCallBack"> task to be executed on destruction with signature</param>
		explicit ScopeGuard(std::function<void()> onDestroyCallBack);

		/// <summary>
		/// set, reset or clear on destruction task
		/// </summary>
		/// <param name="onDestroyCallBack"> on destruction task</param>
		void reset(std::function<void()> onDestroyCallBack = {});


	private:
		/// <summary>
		/// task to be executed on destruction
		/// </summary>
		std::function<void()> mOnDestroyCallback;
	};
}
