#pragma once

#include "CodePeg.h"

#include <array>
#include <vector>
#include <random>

namespace mastermind
{
	/// <summary>
	/// The Code class which represents a code of 4 colored CodePegs.
	/// </summary>
	class Code
	{
	public:
		using ContainerT = std::array<CodePeg, 4>;
		using size_type = ContainerT::size_type;
		using iterator = ContainerT::iterator;
		using const_iterator = ContainerT::const_iterator;
		using reference = ContainerT::reference;
		using const_reference = ContainerT::const_reference;

		/// <summary>
		/// Creates a Code filled with CodePeg::eInvalid
		/// </summary>
		Code();

		/// <summary>
		/// Creates a Code filled with given codePegs
		/// </summary>
		/// <param name="c0">first CodePeg</param>
		/// <param name="c1">second CodePeg</param>
		/// <param name="c2">third CodePeg</param>
		/// <param name="c3">fourth CodePeg</param>
		Code(const CodePeg& c0, const CodePeg& c1, const CodePeg& c2, const CodePeg& c3);

		iterator begin();

		iterator end();

		const_iterator cbegin() const;

		const_iterator cend() const;

		const_iterator begin() const;

		const_iterator end() const;

		CodePeg& operator[](const size_type& index);

		const CodePeg& operator[](const size_type& index) const;

		size_type size() const noexcept;

		friend bool operator==(const Code& lhs, const Code& rhs)
		{
			return lhs.mCodePegs == rhs.mCodePegs;
		}

		friend bool operator!=(const Code& lhs, const Code& rhs)
		{
			return !(lhs == rhs);
		}

		friend bool operator<(const Code& lhs, const Code& rhs)
		{
			return lhs.mCodePegs < rhs.mCodePegs;
		}

		friend bool operator<=(const Code& lhs, const Code& rhs)
		{
			return !(rhs < lhs);
		}

		friend bool operator>(const Code& lhs, const Code& rhs)
		{
			return rhs < lhs;
		}

		friend bool operator>=(const Code& lhs, const Code& rhs)
		{
			return !(lhs < rhs);
		}

		/// <summary>
		/// in-place pre-increment operator to generate the next code  (used for enumerating set of all possible codes)
		/// </summary>
		/// <returns>self reference after incrementation</returns>
		Code& operator++();

		/// <summary>
		/// post-increment operator (see pre-increment)
		/// </summary>
		/// <returns>copy of code before applying the incrementation</returns>
		Code operator++(int);

	private:
		std::array<CodePeg, 4> mCodePegs;
	};


	std::ostream& operator<<(std::ostream& out, const Code& code);

	std::istream& operator>>(std::istream& in, Code& code);

	/// <summary>
	/// generate a uniformly distributed random code using given RandomEngine
	/// </summary>
	/// <typeparam name="RandomEngine"></typeparam>
	/// <param name="engine">a standard STL random engine</param>
	/// <returns>generated random code</returns>
	template <typename RandomEngine>
	Code randomCode(RandomEngine& engine)
	{
		static std::uniform_int_distribution<int> distribution(static_cast<int>(CodePeg::eFirstColor),
		                                                       static_cast<int>(CodePeg::eLastColor));
		Code code;
		std::generate(code.begin(), code.end(),
		              [&]() { return static_cast<CodePeg>(distribution(engine)); });
		return code;
	}

	/// <summary>
	/// Helper to read a valid code from std::cin with user input validation.
	/// </summary>
	/// <returns>valid read code</returns>
	Code readCodeFromConsole();

	/// <summary>
	/// helper to generate vector of all possible code combinations
	/// </summary>
	/// <returns></returns>
	std::vector<Code> generateAllPossibleCodes();
}
