﻿#pragma once

#include "CodeMaker.h"
#include "CodeBreaker.h"
#include <memory>

#include "Factory.h"

namespace mastermind
{
	/// <summary>
	/// Overall game controller which keeps the board, selects type of code makers and breakers via factories and coordinates game play.
	/// </summary>
	class Game
	{
	public:
		/// <summary>
		/// creates game controller and registers the default code makers and breaker (only human and random) and a console view of the game board
		/// </summary>
		Game();

		/// <summary>
		/// register additional code maker
		/// </summary>
		/// <typeparam name="CodeMaker">type of code maker derived from ICodeMaker</typeparam>
		/// <param name="key">unique key identifier used to select code maker</param>
		/// <returns>true if registration was successful, false if key is already in use</returns>
		template <typename CodeMaker>
		bool registerCodeMaker(const std::string& key)
		{
			return mCodeMakerFactory.registerType<CodeMaker>(key);
		}

		/// <summary>
		/// register additional code breaker
		/// </summary>
		/// <typeparam name="CodeBreaker">type of code maker derived from ICodeBreaker</typeparam>
		/// <param name="key">unique key identifier used to select code break</param>
		/// <returns>true if registration was successful, false if key is already in use</returns>
		template <typename CodeBreaker>
		bool registerCodeBreaker(const std::string& key)
		{
			return mCodeBreakerFactory.registerType<CodeBreaker>(key);
		}

		/// <summary>
		/// execute game loop of selection of code maker/breaker, selection of secret, code guessing, game result and loop back
		/// </summary>
		void play();

		/// <summary>
		/// number of guesses done by code breaker
		/// </summary>
		/// <returns>number of guesses</returns>
		std::size_t guessCount() const;

		/// <summary>
		/// checks if code was broken by code breaker
		/// </summary>
		/// <returns>true if code was broken</returns>
		bool isWon() const;

	private:
		/// <summary>
		/// console based user selection of code maker type
		/// </summary>
		void selectCodeMaker();
		
		/// <summary>
		/// console based selection of code breaker type
		/// </summary>
		void selectCodeBreaker();

		/// <summary>
		/// factory for code breakers
		/// </summary>
		Factory<ICodeBreaker, std::string> mCodeBreakerFactory;

		/// <summary>
		/// factory for code makes
		/// </summary>
		Factory<ICodeMaker, std::string> mCodeMakerFactory;

		/// <summary>
		/// current code maker
		/// </summary>
		std::unique_ptr<ICodeMaker> mCodeMaker;

		/// <summary>
		/// current code breaker
		/// </summary>
		std::unique_ptr<ICodeBreaker> mCodeBreaker;

		/// <summary>
		/// board which contains the complete status of the game
		/// </summary>
		Board mBoard;

		/// <summary>
		/// scope guard to unregister lambda which draws board updates
		/// </summary>
		ScopeGuard mBoardView;
	};
}
