﻿#include "KnuthCodeBreaker.h"

#include <map>

namespace mastermind
{
	KnuthCodeBreaker::KnuthCodeBreaker()
	{
		mCombinations = mCandidates = generateAllPossibleCodes();
	}

	void removeCode(std::vector<Code>& set, const Code& code)
	{
		const auto it = std::find(set.begin(), set.end(), code);
		if (it != set.end())
			set.erase(it);
	}


	void KnuthCodeBreaker::makeGuess(Board& board)
	{
		if (board.guesses().empty())
		{
			mCurrentGuess = Code(static_cast<CodePeg>(0), static_cast<CodePeg>(0), static_cast<CodePeg>(1),
			                     static_cast<CodePeg>(1));
		}

		board.makeGuess(mCurrentGuess);
		if (board.isWon())
			return;


		removeCode(mCandidates, mCurrentGuess);
		removeCode(mCombinations, mCurrentGuess);
		pruneCandidates(mCurrentGuess, board.guesses().back().mScore);
		mCurrentGuess = minMaxSearch();
	}

	Code KnuthCodeBreaker::minMaxSearch()
	{
		std::map<Score, int> scoreCount;
		std::map<Code, int> score;
		std::vector<Code> nextGuesses;

		auto less = [](auto&& pairA, auto&& pairB)
		{
			return pairA.second < pairB.second;
		};

		for (auto&& combCode : mCombinations)
		{
			for (auto&& candCode : mCandidates)
			{
				auto pegScore = Board::computeScore(candCode, combCode);
				auto it = scoreCount.find(pegScore);
				if (it != scoreCount.end())
				{
					it->second++;
				}
				else
				{
					scoreCount.emplace(pegScore, 1);
				}
			}

			auto max = std::max_element(scoreCount.begin(), scoreCount.end(), less)->second;
			score.emplace(combCode, max);
			scoreCount.clear();
		}

		const auto min = std::min_element(score.begin(), score.end(), less)->second;

		for (auto&& elem : score)
		{
			if (elem.second == min)
			{
				nextGuesses.push_back(elem.first);
			}
		}

		for (auto&& nextGuess : nextGuesses)
		{
			auto it = std::find(mCandidates.begin(), mCandidates.end(), nextGuess);
			if (it != mCandidates.end())
			{
				return nextGuess;
			}
		}

		for (auto&& nextGuess : nextGuesses)
		{
			auto it = std::find(mCombinations.begin(), mCombinations.end(), nextGuess);
			if (it != mCombinations.end())
			{
				return nextGuess;
			}
		}
		return {};
	}

	void KnuthCodeBreaker::pruneCandidates(const Code& code, const Score& score)
	{
		const auto it = std::remove_if(mCandidates.begin(), mCandidates.end(), [&](auto&& c)
		{
			auto cs = Board::computeScore(code, c);
			return Board::computeScore(code, c) != score;
		});

		mCandidates.erase(it, mCandidates.end());
	}
}
