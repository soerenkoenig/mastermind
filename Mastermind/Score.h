﻿#pragma once

#include <cstddef>
#include <iostream>

namespace mastermind
{
	/// <summary>
	/// The score represents the number of correctly placed CodePegs (black) and the number of valid but incorrectly placed CodePegs (white).
	/// The Score provides equal and relational comparison operators to be usable with 
	/// </summary>
	struct Score
	{
		std::size_t mBlackCount;
		std::size_t mWhiteCount;


		friend bool operator==(const Score& lhs, const Score& rhs)
		{
			return lhs.mBlackCount == rhs.mBlackCount
				&& lhs.mWhiteCount == rhs.mWhiteCount;
		}

		friend bool operator!=(const Score& lhs, const Score& rhs)
		{
			return !(lhs == rhs);
		}

		friend bool operator<(const Score& lhs, const Score& rhs)
		{
			if (lhs.mBlackCount < rhs.mBlackCount)
				return true;
			if (rhs.mBlackCount < lhs.mBlackCount)
				return false;
			return lhs.mWhiteCount < rhs.mWhiteCount;
		}

		friend bool operator<=(const Score& lhs, const Score& rhs)
		{
			return !(rhs < lhs);
		}

		friend bool operator>(const Score& lhs, const Score& rhs)
		{
			return rhs < lhs;
		}

		friend bool operator>=(const Score& lhs, const Score& rhs)
		{
			return !(lhs < rhs);
		}
	};

	std::ostream& operator<<(std::ostream& out, const Score& score);
}
