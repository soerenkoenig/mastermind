﻿#pragma once
#include "Board.h"

#include <random>

namespace mastermind
{
	/// <summary>
	/// Code Maker interface. Defines the API to select a secret code.
	/// </summary>
	class ICodeMaker
	{
	public:
		ICodeMaker() = default;
		virtual ~ICodeMaker() = default;
		ICodeMaker& operator=(const ICodeMaker& other) = delete;
		ICodeMaker& operator=(ICodeMaker&& other) = delete;

		/// <summary>
		/// Select secret must be implemented by concrete CodeMakers the code maker generates the secret code and calls board.setSecret(secretCode).
		/// </summary>
		/// <param name="board">board to set the secret</param>
		virtual void selectSecret(Board& board) = 0;

	protected:
		ICodeMaker(const ICodeMaker& other);
		ICodeMaker(ICodeMaker&& other) noexcept;
	};

	/// <summary>
	/// A random code maker.
	/// </summary>
	class RandomCodeMaker final : public ICodeMaker
	{
	public:
		RandomCodeMaker();

		/// <summary>
		/// selects a random uniformly distributed code and assign it to the given board
		/// </summary>
		/// <param name="board">board to set the secret</param>
		void selectSecret(Board& board) override;

	private:
		std::default_random_engine mRandomEngine;
	};

	/// <summary>
	/// A Human console based code maker which selects the secret code by reading it from the console (std::cin)
	/// </summary>
	class HumanCodeMaker final : public ICodeMaker
	{
	public:
		/// <summary>
		/// Ask user via console input to specify the secret code
		/// </summary>
		/// <param name="board">board to set the secret</param>
		void selectSecret(Board& board) override;
	};
}

