﻿#include "CodeBreaker.h"

namespace mastermind
{
	RandomCodeBreaker::RandomCodeBreaker() : mRandomEngine(std::random_device()())
	{
	}

	void HumanCodeBreaker::makeGuess(Board& board)
	{
		board.makeGuess(readCodeFromConsole());
	}

	void RandomCodeBreaker::makeGuess(Board& board)
	{
		board.makeGuess(randomCode(mRandomEngine));
	}
}
