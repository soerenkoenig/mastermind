﻿#include "Score.h"

namespace mastermind
{
	std::ostream& operator<<(std::ostream& out, const Score& score)
	{
		std::size_t n = 0;
		for (std::size_t i = 0; i < score.mBlackCount; ++i)
		{
			out << "B";
			++n;
		}

		for (std::size_t i = 0; i < score.mWhiteCount; ++i)
		{
			out << "W";
			++n;
		}


		return out;
	}
}
